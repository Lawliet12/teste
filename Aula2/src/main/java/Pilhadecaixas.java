public class Pilhadecaixas {
    private Caixa[] caixas;
    private int topodaPilha;

    public Pilhadecaixas(){
        caixas = new Caixa[5];
        topodaPilha = -1;
    }

    public void empilharCaixas(Caixa c) throws Exception{
        if (estaCheia()){
            throw new Exception("Pilha de caixas está cheia");
        }else{
            topodaPilha++;
            caixas[topodaPilha] = c;
        }

    }
    public Caixa desempilharCaixas() throws Exception{
        if (estaVazia()){
            throw new Exception("Pilha de de caixas está vazia");
        }else{
            Caixa c;
            c = caixas[topodaPilha];
            topodaPilha--;
            return c;
        }
    }
    public boolean estaCheia(){
        if (topodaPilha == caixas.length - 1){
            return true;
        }else{
            return false;
        }
    }
    public boolean estaVazia(){
        if (topodaPilha == - 1){
            return true;
        }else{
            return false;
        }
    }

    public Caixa[] getCaixas() {
        return caixas;
    }

    public void setCaixas(Caixa[] caixas) {
        this.caixas = caixas;
    }

    public int getTopodaPilha() {
        return topodaPilha;
    }

    public void setTopodaPilha(int topodaPilha) {
        this.topodaPilha = topodaPilha;
    }


}

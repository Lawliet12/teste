public class Fila {
    private Carro[] carros;
    private int inicio;
    private int fim;
    private int max;

    public Fila (int taman){
        carros = new Carro[taman];
        inicio = 0;
        fim = 0;
        max = taman - 1;
    }
    public void Queue(Carro novoCarro)throws Exception{
        if (inicio >= 0 && inicio <= max) {
            carros[inicio] = novoCarro;
            inicio++;
        }else{
            throw new Exception("Fila atingiu capacidade máxima");
        }
    }
    public Carro DeQueue()throws Exception{
        if (! Vazia()){
            Carro c;
            c = carros[fim];
            fim++;
            return c;
        }else{
            throw new Exception("Fila está vazia");
        }
    }
    public boolean Vazia(){
        if (inicio == 0){
            return true;
        }else {
            return false;
        }
    }
    public int Tamanho(){
        return 0;
    }

    public Carro[] getCarros() {
        return carros;
    }

}

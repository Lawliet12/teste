public class Caixa {
    private String nomedaBebida;

    public Caixa(String nome){
        nomedaBebida = nome;
    }

    public String getNomedaBebida() {
        return nomedaBebida;
    }

    public void setNomedaBebida(String nomedaBebida) {
        this.nomedaBebida = nomedaBebida;
    }

    @Override
    public String toString() {
        return "Caixa de : " + getNomedaBebida();
    }
}

import java.util.Scanner;

public class Teste {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        Pilhadecaixas p = new Pilhadecaixas();
        Deposito d = new Deposito();
        int opcao;
        do{
            System.out.println("1 - Incluir uma nova caixa de bebidas na pilha");
            System.out.println("2 - Consumir uma nova caixa de bebidas");
            System.out.println("3 - Descartar um lote inteiro");
            System.out.println("4 - Alertar o gerente quando o deposito alcançar 80% da sua capacidade");
            System.out.println("5 - Sair");
            System.out.print("Digite a opção desejada: ");
            opcao = ler.nextInt();
            switch (opcao){
                case 1:
                    Caixa c = new Caixa("Cerveja");
                    try {
                        p.empilharCaixas(c);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    if (p.estaCheia()){
                        try {
                            d.enfileirar(p.getCaixas());
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                case 2:
                    try{
                       d.desenfileirar();

                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }

            }
        }while (opcao != 5);

    }
}

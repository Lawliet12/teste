public interface IPilha {
    void inicializar(int qtdElementos);
    void empilhar(long dado);
    void desempinhar();
    boolean estaVazia();
    boolean estaCheia();
}


public class Deposito {
    private Object[] fila;
    private int inicio;
    private int fim;
    private int total;

    public Deposito(){
        fila = new Object[10];
        inicio = 0;
        fim = 0;
        total = 0;
    }
    public void enfileirar(Object o)throws Exception{
        if (isFull()){
            throw new Exception("Deposito cheio");
        }else{
            fila[fim] = o;
            fim = (fim + 1) % fila.length;
            total++;
        }

    }
    public Object desenfileirar()throws Exception{
        if (isEmpty()){
            throw new Exception("Deposito vazio");
        }else{
            Object o = fila[inicio];
            inicio = (inicio + 1) % fila.length;
            total--;
            return o;
        }
    }
    public boolean isEmpty(){
        return total == 0;
    }
    public boolean isFull(){
        return total == fila.length;
    }




}

import java.util.Scanner;

public class TesteFila {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        System.out.print("Digite o tamanho da fila");
        Fila fila = new Fila(ler.nextInt());
        char opcao;
        do{
            System.out.print("Digite o nome do carro: ");
            String nome = ler.next();
            System.out.print("Digite o ano do carro: ");
            int ano = ler.nextInt();
            Carro carro = new Carro(nome, ano);
            try {
                fila.Queue(carro);
            }catch (Exception e){
                System.out.println(e.getMessage());
                break;

            }
            System.out.println("Deseja cadastrar outro carro(s/n): ");
            opcao = ler.next().charAt(0);
        }while(opcao != 'n');
        for (int i = 0; i < fila.getCarros().length; i++){
            try {
                System.out.println(fila.DeQueue());
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
}

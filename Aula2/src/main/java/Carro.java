public class Carro {
    private String nome;
    private int ano;

    public Carro(String nome, int ano){
        setNome(nome);
        setAno(ano);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        return "Nome do carro: " + getNome() + ", ano do carro: " + getAno() + "; ";
    }
}

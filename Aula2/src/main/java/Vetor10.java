import java.util.Arrays;
import java.util.Scanner;

public class Vetor10 {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int[] numeros = new int[10];
        int aux;
        for (int i = 0; i < numeros.length; i++){
            System.out.print("Digite um número: ");
            numeros[i] = ler.nextInt();
        }
        for (int i = 0; i < numeros.length; i++){
            for (int j = 0; j < (numeros.length - 1); j++){
                if (numeros[j] > numeros[j + 1]){
                    aux = numeros[j + 1];
                    numeros[j + 1] = numeros[j];
                    numeros[j] = aux;
                }
            }
        }
        System.out.println("Forma crescente");
        System.out.println(Arrays.toString(numeros));
        for (int i = 0; i < numeros.length; i++){
            for (int j = 0; j < (numeros.length - 1); j++){
                if (numeros[j] < numeros[j + 1]){
                    aux = numeros[j + 1];
                    numeros[j + 1] = numeros[j];
                    numeros[j] = aux;
                }
            }
        }
        System.out.println("Forma decrescente");
        System.out.println(Arrays.toString(numeros));
    }
}
